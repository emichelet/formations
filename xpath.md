---
Contributeur.ice.s: Guillaume Porte
Date de création: 2023
Modificatons: 2024
Titre: XPATH
Licence: CC BY-SA
Description: Ce document est un support destiné à une formation en présentiel avec un.e animateur.ice. Il ne s'agit pas d'un tutoriel.
---

**(en cours de rédaction)**

# XPATH

## L’arbre XML
![arbre XML](https://cdn2.hubspot.net/hubfs/4367560/tree-7.png)
(image : https://www.zyte.com/blog/an-introduction-to-xpath-with-examples/)


## Syntaxe XPATH 

à voir ici [https://www.w3schools.com/xml/xpath_intro.asp](https://www.w3schools.com/xml/xpath_intro.asp)


### Principes généraux

On commence en général l’instruction par un « / », qui indique la racine du document.

`/` => 1 niveau en dessous (child)

- `/div` => tous les éléments `<div>` situés à la racine du document
- `/div/head` => tous les éléments `<head>` enfants d’un élément `<div>` situé à la racine du document

`//` => tous les niveaux en dessous (descendant)

- `//div` => n’importe quel élément `<div>` dans le document
- `//div//head` => tous les éléments `<head>` enfants ou descendants d’un élément `<div>` situé à n’importe quel endroit du document

`*` => n’importe quel élément, quel que soit son nom

`//div/*` => n’importe quel enfant d’un élément `<div>`

@ => sélectionne un attribut

`/div/@type` => attribut "type" de l’élément `<div>`

`.` => dernier élément sélectionné

### parenté 

`child::` => 1 niveau en dessous (identique à `/`)

- `/div/child::head` = `/div/head`

`descendant::` => n’importe quel niveau en dessous (identique à `//`)

- `/div/descendant::head` = `/div//head`

`parent::` => 1 niveau au dessus du l’élément précédent

- //head/parent::div = le `<div>` qui est le parent d’un `<head>`
      
`ancestor::` => n’importe quel niveau au dessus de l’élément précédent

- `//head/ancestor::div` = tous les `<div>` dans lesquels est contenu un  `<head>`

`following::*` ou `preceding::*` => tous les éléments situés après (following) ou avant (preceding) l’élément précédent, quelque soit leur position dans le document

`following-sibling::*` ou `preceding-sibling::*`  => tous les éléments situés après ou avant l’élément précédent et qui ont le même parent


### prédicats

`/div/p[1]` => sélectionne le 1er élément `<p>` d’un élément `<div>`

`/div[@type]` => sélectionne les éléments `<div>` qui possèdent un attribut @type

`/div[@type="chapitre"]` => sélectionne les éléments `<div>` qui possèdent un attribut @type dont la valeur est "chapitre"

`/div[date]` => sélectionne les éléments `<div>` qui possèdent un ou des enfants `<date>`

`/div[date>`2020]` => sélectionne les éléments `<div>` qui possèdent un ou des enfants `<date>` dont la valeur est supérieur à 2020

`/p[contains(.,"blablabla")]` => sélection les éléments `<p>` qui contiennent le texte « blablabla »


### opérateurs :

- `>` (supérieur)
- `>=` (supérieur ou égal)
- `<` (inférieur)
- `=<` (inférieur ou égal)
- `=` égal
- `!=` différent 
- `or` (ou => `date>2020` or `date<2023`)
- `and`  (et => `date>2020` and `date<2023`)

## Essayez !

1. allez sur http://xpather.com/

2. copiez/collez le texte contenu dans le fichier bookstore.xml dans le bloc de gauche

_**note** => les XPATH sont à construire dans la barre du haut, les résultats (éléments sélectionnés) sont surlignés et sont aussi détaillés dans la colonne de droite_

3. on peut essayer quelques XPATH :

    - sélectionnez l’élément `<title>` de chaque élément `<book>`
    - sélectionnez l’élément `<title>` de chaque élément `<book>` dont l’attribut @category est « web ».
    - sélectionnez les éléments `<book>` dont l’élément `<price>` est supérieur à 20
    - sélectionnez les éléments `<book>` dont un des éléments `<author>` contient « Jaworski »
    - sélectionnez l’attribut @lang de l’élément `<title>` pour un `<book>` dont l’élément `<year>` est inférieur à 2000

4. dans la colonne de gauche, supprimez le contenu et copiez/collez le contenu du fichier `pantagruel.xml`

5. regardez bien la structure du document et sélectionnez les titres des chapitres de l’ouvrage