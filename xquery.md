---
Contributeur.ice.s: Guillaume Porte
Date de création: 2023
Modifications: 2024-05
Titre: XQuery
Licence: CC BY-SA
Description: support pour une formation à XQuery niveau débutant. "XQuery est un langage de requête informatique permettant non seulement d'extraire des informations d'un document XML [...] [C'est] une spécification du W3C dont la version 1.0 finale date de janvier 2007 [...]" (d'après https://fr.wikipedia.org/wiki/XQuery)
---

**(en cours de rédaction)**


# XQuery

1. ouvrez Basex GUI

2. dans la  barre de menu suivante, sélectionnez uniquement le crayon et le `<x>`

![graphic](Pictures/10000000000002020000002852297DEEA2A140D5.png "graphic")

3. dans la barre de menu suivante, cliquez sur le logo page (ou `Editor > New` ou `ctrl+t` ou `cmd+t`)

![graphic](Pictures/100000000000010F000000265C3BB29224DEBA81.png "graphic")

4. dans l’éditeur qui vient de s’ouvrir, insérer les lignes suivantes :

```
let $document := doc("DossierDeFormation/bookstore.xml")
return($document)
```
    - `let $document :=` créé une _variable_ ([quésaco](https://fr.wikipedia.org/wiki/Variable_(informatique))?) nommée « $document » dont le contenu est celui du document « boostore.xml »
    - `doc()` charge le document contenu dans les parenthèsesreturnrenvoie le résultat
    - `doc()` permet aussi de charger un document en ligne, par exemple : 

```
let $document := doc("https://num-arche.unistra.fr/tjem/editions/admm_b_7835.xml/tei")
return($document)
```

5. parcourir un arbre XML avec for :

```
let $document := doc("DossierDeFormation/bookstore.xml")
for $livre in $document//*:book
return($livre)
```

`for $livre in $document//*:book` : à chaque fois que l’expression trouve un élément `<book>` dans `$document`, elle créée une variable `$livre` qui contient l’élément `<book>`

6. on teste ! => pour chaque `<book>`, affichez  (`return`) la valeur de `<title>`

7. trier les résultats :

```
let $document := doc("DossierDeFormation/bookstore.xml")
for $livre in $document//*:book
let $titre := $livre/*:title
order by $titre
return($livre)
```

`order by` : effectue un tri. On peut préciser « ascending » (par défaut) ou « descending » après la variable

8. on teste ! => ajouter une variable pour l’élément `<author>` et triez  les résultats par `<surname>`

9. comparez les résultats en changeant le contenu du return

    - a : `$livre/*:title`
    - b : `$titre`
    - c : `data($titre)`

10. concaténer des résultats

`return('livre : ' || $titre )`

11. on teste ! => renvoyer les résultats sous cette forme « Livre : livre, année. »

(note : ici l’italique indique simplement où sont les variables)

12. filtrer les résultats

```
let $document := doc("DossierDeFormation/bookstore.xml")
for $livre in $document//*:book
where $livre/year > 2000
let $titre := $livre/*:title
order by $titre
return($titre || ', ' || $livre/*:year)
```

`where` : ajoute un filtre à la requête : on cherche tous les éléments qui répondent au critère défini dans « where »

13. on teste ! => cherchez tous les livres dont le prix est inférieur à 30, triez-les par année et affichez les résultats sous la forme « auteur, titre, année (prix) ».

_Un problème avec l’affichage du nom d’auteur ?_

14. XQuery peut utiliser de nombreuses fonctions utiles ([wàs isch](https://fr.wikipedia.org/wiki/Fonction#Mathématiques_et_informatique)?) comme `count()`, `replace()`, `upper-case()` et beaucoup d’autres (sachant qu’on peut aussi en créer soi-même).

Réessayez le précédent exercice en utilisant la fonction `string-join()` pour le résultat du nom d’auteur :

`string-join($NomDeVotreVariableAuteur/*, ' ')`

Puis on peut en tester d’autres :

```
let $document := doc("DossierDeFormation/bookstore.xml")
let $nombreLivres := count($document//*:book)
return($nombreLivres)
```

Ou encore :

```
let $document := doc("DossierDeFormationbookstore.xml)
for $livre in $document//*:book
return(upper-case($livre//*:author) || ", " || replace($livre//*:title, "e", "€"))
```

15. Générer du HTML avec XQuery
```
<div>
<h1>Ma liste de livres</h1>
<ul>{let $document := doc("DossierDeFormation/bookstore.xml")for $livre in $document//*:booklet $auteur := $livre/*:authorlet $titre := $livre/*:titlelet $annee := $livre/*:yearreturn(<li>{string-join($auteur/*, ' ')}, <em>{data($titre)}</em></li>)}</ul>
</div>
```

15. Générer du CSV avec XQuery

```
"auteur;livre;année",(

let $document := doc("DossierDeFormation/bookstore.xml")

for $livre in $document//*:book

let $auteur := $livre/*:author

let $titre := $livre/*:title

let $annee := $livre/*:year

return

(

string-join($auteur/*, ' ') || ';' || $titre || ';' || $annee

)

)
```